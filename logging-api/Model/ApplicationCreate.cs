﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Model
{
    public class ApplicationCreate
    {
        [Required(ErrorMessage = "An application name is required.")]
        [MaxLength(150, ErrorMessage = "An application name cannot be longer than 150 characters.")]
        public string Name { get; set; }
    }
}
