﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Model
{
    public class LogEntryCreate
    {
        public string LogLevel { get; set; } = "INFO";

        public string LogMessage { get; set; }
    }
}
