﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Model
{
    public class ApplicationSecure : Application
    {
        public Guid ApiKey { get; set; }
    }
}
