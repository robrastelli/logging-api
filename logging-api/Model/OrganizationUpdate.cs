﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Model
{
    public class OrganizationUpdate
    {
        [Required(ErrorMessage = "Organization Name is required to update.")]
        [MaxLength(150, ErrorMessage = "Organization name must be less than 150 characters in length.")]
        public string Name { get; set; }
    }
}
