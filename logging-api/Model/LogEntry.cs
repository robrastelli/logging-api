﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Model
{
    public class LogEntry
    {
        public Guid Id { get; set; }

        public string LogLevel { get; set; } = "INFO";

        public DateTimeOffset LogDate { get; set; } = DateTime.UtcNow;

        public string LogMessage { get; set; }
    }
}
