﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Model
{
    public class Role
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
