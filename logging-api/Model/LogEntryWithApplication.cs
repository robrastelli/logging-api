﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Model
{
    public class LogEntryWithApplication : LogEntry
    {
        public Application Application { get; set; }
    }
}
