﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Model
{
    public class OrganizationCreate
    {
        [Required(ErrorMessage = "Organization Name is required to create.")]
        [MaxLength(150, ErrorMessage = "Organization name must be less than 150 characters in length.")]
        public string Name { get; set; }

        public IEnumerable<ApplicationCreate> Applications { get; set; } = new List<ApplicationCreate>();
    }
}
