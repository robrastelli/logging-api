﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using logging_api.DataAccess.Services;
using Entities = logging_api.data.authentication.DataAccess.Entities;
using Dto = logging_api.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace logging_api.Controllers
{
    [Route("api/organizations")]
    [ApiController]
    [Authorize]
    public class OrganizationsController : ControllerBase
    {
        private readonly IAuthenticationService _authService;
        private readonly IMapper _mapper;

        public OrganizationsController(IAuthenticationService authService, IMapper mapper)
        {
            _authService = authService ?? throw new ArgumentNullException(nameof(authService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet(Name = "GetOrganizations")]
        public ActionResult<IEnumerable<Dto.Organization>> GetOrganizations()
        {
            var organizationsFromDb = _authService.GetOrganizations();

            return Ok(_mapper.Map<IEnumerable<Dto.Organization>>(organizationsFromDb));
        }

        [HttpGet("{organizationId:guid}", Name = "GetOrganization")]
        public ActionResult<Dto.Organization> GetOrganization([FromRoute]Guid organizationId)
        {
            var organizationFromDb = _authService.GetOrganization(organizationId);

            return Ok(_mapper.Map<Dto.Organization>(organizationFromDb));
        }

        [HttpPost(Name = "CreateOrganization")]
        [Authorize(Roles = "Admin")]
        public ActionResult<Dto.Organization> CreateOrganization([FromBody]Dto.OrganizationCreate organization)
        {
            var organizationEntity = _mapper.Map<Entities.ApiOrganization>(organization);
            _authService.AddOrganization(organizationEntity);
            _authService.Save();

            var returnOrganization = _mapper.Map<Dto.Organization>(organizationEntity);

            return CreatedAtRoute("GetOrganization", new { organizationId = returnOrganization.Id }, returnOrganization);
        }

        [HttpDelete("{organizationId:guid}", Name = "DeleteOrganization")]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteOrganization([FromRoute]Guid organizationId)
        {
            var organizationFromRepo = _authService.GetOrganization(organizationId);

            if (organizationFromRepo == null)
                return NotFound();

            _authService.DeleteOrganization(organizationFromRepo);
            _authService.Save();

            return NoContent();
        }

        [HttpPut("{organizationId:guid}", Name = "UpdateOrganization")]
        [Authorize(Roles = "Admin")]
        public ActionResult UpdateOrganization([FromRoute]Guid organizationId, [FromBody]Dto.OrganizationUpdate organization)
        {
            var organizationFromDb = _authService.GetOrganization(organizationId);

            if (organizationFromDb == null)
                return NotFound();

            _mapper.Map(organization, organizationFromDb);

            _authService.UpdateOrganization(organizationFromDb);
            _authService.Save();

            return NoContent();
        }
    }
}