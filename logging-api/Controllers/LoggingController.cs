﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using logging_api.DataAccess.Services;
using Dto = logging_api.Model;
using Entities = logging_api.data.logging.DataAccess.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace logging_api.Controllers
{
    [Route("api/logging")]
    [ApiController]
    [Authorize]
    public class LoggingController : ControllerBase
    {
        private readonly ILoggingService _loggingService;
        private readonly IAuthenticationService _authService;
        private readonly IMapper _mapper;

        public LoggingController(ILoggingService loggingService, IAuthenticationService authService, IMapper mapper)
        {
            _loggingService = loggingService ?? throw new ArgumentNullException(nameof(loggingService));
            _authService = authService ?? throw new ArgumentNullException(nameof(authService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet(Name = "GetLogEntries")]
        public ActionResult<IEnumerable<Dto.LogEntry>> GetLogEntries([FromHeader(Name = "X-Api-Key")]string apiKey)
        {
            var application = _authService.GetApplicationByApiKey(apiKey);

            if (application == null)
                return NotFound();

            var entriesFromDb = _loggingService.GetLogsForApplication(application.Id);

            return Ok(_mapper.Map<IEnumerable<Dto.LogEntry>>(entriesFromDb));
        }

        [HttpGet("{logEntryId}", Name = "GetLogEntry")]
        public ActionResult<Dto.LogEntry> GetLogEntry([FromRoute]Guid logEntryId, [FromHeader(Name = "X-Api-Key")]string apiKey)
        {
            var application = _authService.GetApplicationByApiKey(apiKey);

            if (application == null)
                return NotFound();

            var entryFromDb = _loggingService.GetLogForApplication(application.Id, logEntryId);

            if (entryFromDb == null)
                return NotFound();

            return Ok(_mapper.Map<Dto.LogEntry>(entryFromDb));
        }

        [HttpPost(Name = "CreateLogEntry")]
        public ActionResult<Dto.LogEntry> CreateLogEntry([FromBody]Dto.LogEntryCreate logEntry, [FromHeader(Name = "X-Api-Key")]string apiKey)
        {
            var application = _authService.GetApplicationByApiKey(apiKey);

            if (application == null)
                return NotFound();

            var logEntryEnttiy = _mapper.Map<Entities.LogEntry>(logEntry);

            logEntryEnttiy.ApplicationId = application.Id;
            logEntryEnttiy.LogDate = DateTime.UtcNow;

            _loggingService.AddLogEntry(logEntryEnttiy);
            _loggingService.Save();

            var returnEntry = _mapper.Map<Dto.LogEntry>(logEntryEnttiy);

            return CreatedAtRoute("GetLogEntries", new { logEntryId = returnEntry.Id }, returnEntry);
        }
    }
}