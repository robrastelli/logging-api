﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using logging_api.DataAccess.Services;
using Dto = logging_api.Model;
using Entities = logging_api.data.authentication.DataAccess.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace logging_api.Controllers
{
    [Route("api/organizations/{organizationId}/applications")]
    [ApiController]
    [Authorize]
    public class ApplicationsController : ControllerBase
    {
        private readonly IAuthenticationService _authService;
        private readonly IMapper _mapper;

        public ApplicationsController(IAuthenticationService authService, IMapper mapper)
        {
            _authService = authService ?? throw new ArgumentNullException(nameof(authService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet(Name = "GetApplications")]
        public ActionResult<IEnumerable<Dto.Application>> GetApplications([FromRoute]Guid organizationId, [FromHeader(Name = "X-Api-Key")]string apiKey)
        {
            if (!_authService.OrganizationExists(organizationId))
                return NotFound();

            var applicationsFromDb = _authService.GetApplicationsForOrganization(organizationId);

            var returnList = new List<Dto.Application>();

            if (applicationsFromDb.Any(a => a.ApiKey == apiKey))
                returnList.AddRange(_mapper.Map<IEnumerable<Dto.ApplicationSecure>>(applicationsFromDb));
            else
                returnList.AddRange(_mapper.Map<IEnumerable<Dto.Application>>(applicationsFromDb));

            return Ok(returnList);
        }

        [HttpGet("{applicationId:guid}", Name = "GetApplication")]
        public ActionResult<Dto.Application> GetApplication([FromRoute]Guid organizationId, [FromRoute]Guid applicationId, [FromHeader(Name = "X-Api-Key")]string apiKey)
        {
            if (!_authService.OrganizationExists(organizationId))
                return NotFound();

            var applicationFromDb = _authService.GetApplicationForOrganization(organizationId, applicationId);

            if (applicationFromDb == null)
                return NotFound();

            Dto.Application returnApplication;

            if (applicationFromDb.ApiKey == apiKey)
                returnApplication = _mapper.Map<Dto.ApplicationSecure>(applicationFromDb);
            else
                returnApplication = _mapper.Map<Dto.Application>(applicationFromDb);

            return returnApplication;
        }
    }
}