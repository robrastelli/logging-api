﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using logging_api.DataAccess.Services;
using logging_api.Helpers;
using Dto = logging_api.Model;
using logging_api.ResourceParameters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Dynamic;

namespace logging_api.Controllers
{
    [Route("api/log")]
    [ApiController]
    [Authorize]
    public class LogController : ControllerBase
    {
        public readonly ILoggingService _loggingService;
        public readonly IAuthenticationService _authService;
        public readonly IMapper _mapper;

        public LogController(ILoggingService loggingService, IAuthenticationService authService, IMapper mapper)
        {
            _loggingService = loggingService ?? throw new ArgumentNullException(nameof(loggingService));
            _authService = authService ?? throw new ArgumentNullException(nameof(authService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet(Name = "GetLogs")]
        public IActionResult GetLogs([FromQuery]LogResourceParameters logParams, [FromHeader(Name = "X-Api-Key")]string apiKey)
        {
            var applicationFromDb = _authService.GetApplicationByApiKey(apiKey);

            if (applicationFromDb == null)
                return NotFound();

            var organizationFromDb = _authService.GetOrganization(applicationFromDb.OrganizationId);

            if (organizationFromDb == null)
                return NotFound();

            var applicationIds = _authService.GetApplicationsForOrganization(organizationFromDb.Id).Select(a => a.Id).ToList();

            var logsFromDb = _loggingService.GetLogsForApplications(applicationIds, logParams);

            var paginationMetadata = new
            {
                totalCount = logsFromDb.TotalCount,
                pageSize = logsFromDb.PageSize,
                currentPage = logsFromDb.CurrentPage,
                totalPages = logsFromDb.TotalPages
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(paginationMetadata));

            var links = createLinksForLogs(logParams, logsFromDb.HasNext, logsFromDb.HasPrevious);

            var returnEntries = _mapper.Map<IEnumerable<Dto.LogEntry>>(logsFromDb);

            return Ok(new { values = returnEntries, links });
        }

        private IEnumerable<Dto.Link> createLinksForLogs(LogResourceParameters logParams, bool hasNext, bool hasPrevious)
        {
            var links = new List<Dto.Link>();

            // Self Link
            links.Add(new Dto.Link(Url.Link("GetLogs", new
            {
                pageNumber = logParams.PageNumber,
                pageSize = logParams.PageSize,
                logType = logParams.LogType,
                fields = logParams.Fields
            }), "self", "GET"));

            // Next Link
            if (hasNext)
            {
                links.Add(new Dto.Link(Url.Link("GetLogs", new
                {
                    pageNumber = logParams.PageNumber + 1,
                    pageSize = logParams.PageSize,
                    logType = logParams.LogType,
                    fields = logParams.Fields
                }), "next", "GET"));
            }

            // Previous Link
            if (hasPrevious)
            {
                links.Add(new Dto.Link(Url.Link("GetLogs", new
                {
                    pageNumber = logParams.PageNumber - 1,
                    pageSize = logParams.PageSize,
                    logType = logParams.LogType,
                    fields = logParams.Fields
                }), "self", "GET"));
            }

            return links;
        }
    }
}