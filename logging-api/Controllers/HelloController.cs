﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using logging_api.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace logging_api.Controllers
{
    [Route("api/hello")]
    [ApiController]
    public class HelloController : ControllerBase
    {
        [HttpGet("hello-anon")]
        public IActionResult HelloAnonymous()
        {
            return Ok($"Hello anonymous user from {nameof(HelloAnonymous)}");
        }

        [HttpGet("hello-auth")]
        [Authorize]
        public IActionResult HelloAuthorized([FromHeader(Name = "X-Api-Key")]string apiKey)
        {
            var user = User.Identity.Name;
            return Ok($"Hello authorized user {user} from {nameof(HelloAuthorized)} using API Key {apiKey}");
        }
    }
}