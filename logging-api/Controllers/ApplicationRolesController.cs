﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using logging_api.DataAccess.Services;
using Dto = logging_api.Model;
using Entities = logging_api.data.authentication.DataAccess.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace logging_api.Controllers
{
    [Route("api/organizations/{organizationId:guid}/applications/{applicationId:guid}/roles")]
    [ApiController]
    [Authorize]
    public class ApplicationRolesController : ControllerBase
    {
        private readonly IAuthenticationService _authService;
        private readonly IMapper _mapper;

        public ApplicationRolesController(IAuthenticationService authService, IMapper mapper)
        {
            _authService = authService ?? throw new ArgumentNullException(nameof(authService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet(Name = "GetApplicationRoles")]
        public ActionResult<IEnumerable<Dto.Role>> GetRolesForApplication([FromRoute]Guid organizationId, [FromRoute]Guid applicationId)
        {
            if (!_authService.OrganizationExists(organizationId))
                return NotFound();

            if (!_authService.ApplicationExistsForOrganization(organizationId, applicationId))
                return NotFound();

            var rolesFromDb = _authService.GetRolesForApplication(applicationId);

            return Ok(_mapper.Map<IEnumerable<Dto.Role>>(rolesFromDb));
        }
    }
}