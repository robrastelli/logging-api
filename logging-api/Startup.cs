using System;
using AutoMapper;
using logging_api.Authentication;
using logging_api.data.authentication.DataAccess.Contexts;
using Services = logging_api.DataAccess.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Http;
using logging_api.data.logging.DataAccess.Contexts;
using Microsoft.Extensions.Logging;
using logging_api.Middleware;

namespace logging_api
{
    public class Startup
    {
        private const string _MyOriginsPolicy = "_AllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(name: _MyOriginsPolicy,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200").AllowAnyHeader().WithMethods("OPTIONS", "GET", "POST");
                    });
            });

            services.AddControllers(setupAction =>
            {
                setupAction.ReturnHttpNotAcceptable = true;
            })
                .AddNewtonsoftJson(setupAction =>
                {
                    setupAction.SerializerSettings.ContractResolver =
                        new CamelCasePropertyNamesContractResolver();
                })
                .ConfigureApiBehaviorOptions(setupAction =>
                {
                    setupAction.InvalidModelStateResponseFactory = context =>
                    {
                        // create a problem details object
                        var problemDetailFactory = context.HttpContext.RequestServices
                            .GetRequiredService<ProblemDetailsFactory>();
                        var problemDetails = problemDetailFactory.CreateValidationProblemDetails(
                            context.HttpContext,
                            context.ModelState);

                        // Add additional info not added be default
                        problemDetails.Detail = "See the errors field for details.";
                        problemDetails.Instance = context.HttpContext.Request.Path;

                        // find out which status code to use
                        var actionExecutingContext = context as Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext;

                        if ((context.ModelState.ErrorCount > 0) &&
                            (actionExecutingContext?.ActionArguments.Count == context.ActionDescriptor.Parameters.Count))
                        {
                            problemDetails.Type = "https://httpstatuses.com/422";
                            problemDetails.Status = StatusCodes.Status422UnprocessableEntity;
                            problemDetails.Title = "One or more validation errors occurred.";

                            return new UnprocessableEntityObjectResult(problemDetails)
                            {
                                ContentTypes = { "application/problem+json" }
                            };
                        }

                        // if one of the arguments wasn't correclty found / couldn't be parsed
                        // we're dealing with null/unparsable input
                        problemDetails.Type = "https://httpstatuses.com/400";
                        problemDetails.Status = StatusCodes.Status400BadRequest;
                        problemDetails.Title = "One or more errors on input occurred.";
                        return new BadRequestObjectResult(problemDetails)
                        {
                            ContentTypes = { "application/problem+json" }
                        };
                    };
                });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = ApiKeyAuthenticationOptions.DefaultScheme;
                options.DefaultChallengeScheme = ApiKeyAuthenticationOptions.DefaultScheme;
            })
                .AddApiKeySupport(options => { });

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddScoped<Services.IAuthenticationService, Services.AuthenticationService>();
            services.AddScoped<Services.ILoggingService, Services.LoggingService>();

            services.AddTransient<IAuthenticationHandler, ApiKeyAuthenticationHandler>();

            services.AddTransient<IGetApiKeyQuery, DBMemoryGetApiKeyQuery>();

            services.AddDbContext<AuthenticationContext>(options =>
            {
                options.UseSqlServer(
                    Configuration.GetConnectionString("AuthenticationContext"),
                    sqlServerOptions =>
                    {
                        sqlServerOptions.MigrationsAssembly("logging-api.data.authentication");
                    });
            });

            services.AddDbContext<LoggingContext>(options =>
            {
                options.UseSqlServer(
                    Configuration.GetConnectionString("LoggingContext"),
                    sqlServerOptions =>
                    {
                        sqlServerOptions.MigrationsAssembly("logging-api.data.logging");
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();
            app.UseRequestLogger();

            app.UseProblemDetails();

            app.UseRouting();

            app.UseCors(_MyOriginsPolicy);

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
