﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace logging_api.Middleware
{
    public class ProblemDetailsMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ProblemDetailsMiddleware(RequestDelegate next, ILogger<ProblemDetailsMiddleware> logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Invoke(HttpContext context)
        {
            await _next(context);

            if (context.Response != null && context.Response.StatusCode >= 400 && context.Response.StatusCode < 500)
            {
                var responseBody = JsonSerializer.Serialize(new ProblemDetails
                {
                    Type = $"https://httpstatuses.com/{context.Response?.StatusCode}",
                    Status = context.Response?.StatusCode,
                    Title = "One or more errors occurred."
                });

                var swBody = new StreamWriter(context.Response?.Body);

                context.Response.ContentType = "application/problem+json";
                context.Response.ContentLength = responseBody.Length;

                await swBody.WriteAsync(responseBody);

                await swBody.FlushAsync();
            }
        }
    }
}
