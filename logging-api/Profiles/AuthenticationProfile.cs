﻿using AutoMapper;
using Entities = logging_api.data.authentication.DataAccess.Entities;
using Dto = logging_api.Model;

namespace logging_api.Profiles
{
    public class AuthenticationProfile : Profile
    {
        public AuthenticationProfile()
        {
            CreateMap<Entities.ApiOrganization, Dto.Organization>();
            CreateMap<Dto.OrganizationCreate, Entities.ApiOrganization>();
            CreateMap<Dto.OrganizationUpdate, Entities.ApiOrganization>();

            CreateMap<Entities.ApiApplication, Dto.Application>();
            CreateMap<Entities.ApiApplication, Dto.ApplicationSecure>();
            CreateMap<Dto.ApplicationCreate, Entities.ApiApplication>();

            CreateMap<Entities.ApiRole, Dto.Role>();
        }
    }
}
