﻿using AutoMapper;
using Entities = logging_api.data.logging.DataAccess.Entities;
using Dto = logging_api.Model;

namespace logging_api.Profiles
{
    public class LoggingProfile : Profile
    {
        public LoggingProfile()
        {
            CreateMap<Entities.LogEntry, Dto.LogEntry>();

            CreateMap<Dto.LogEntryCreate, Entities.LogEntry>();
        }
    }
}
