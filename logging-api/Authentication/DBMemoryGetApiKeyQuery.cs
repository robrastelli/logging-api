﻿using logging_api.DataAccess.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Authentication
{
    public class DBMemoryGetApiKeyQuery : IGetApiKeyQuery
    {
        private readonly IAuthenticationService _authService;

        public DBMemoryGetApiKeyQuery(IAuthenticationService authService)
        {
            _authService = authService ?? throw new ArgumentNullException(nameof(authService));
        }

        public Task<ApiKey> Execute(string providedApiKey)
        {
            var applicationFromRepo = _authService.GetApplicationByApiKey(providedApiKey);

            if (applicationFromRepo == null)
            {
                return Task.FromResult<ApiKey>(null);
            }

            var rolesFromRepo = _authService.GetRolesForApplication(applicationFromRepo.Id);

            var key = new ApiKey(applicationFromRepo.Id, applicationFromRepo.Name, applicationFromRepo.ApiKey, applicationFromRepo.CreateDate, rolesFromRepo.Select(r => r.Name).ToList());

            return Task.FromResult(key);
        }
    }
}
