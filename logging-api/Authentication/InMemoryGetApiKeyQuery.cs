﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

#warning This should not be released into Production!

namespace logging_api.Authentication
{
    public class InMemoryGetApiKeyQuery : IGetApiKeyQuery
    {
        private readonly IDictionary<string, ApiKey> _apiKeys;

        public InMemoryGetApiKeyQuery()
        {
            var existingApiKeys = new List<ApiKey>
            {
                new ApiKey(Guid.NewGuid(), "NJ SMART", "08566f35-7c77-4f4b-840f-a4e53415e21c", DateTime.Now, new List<string> { "FrontEndUi" })
#if DEBUG
                , new ApiKey(Guid.NewGuid(), "Development API Key", "TEST-API-KEY", DateTime.Now, new List<string> { "TestUser" })
#endif
            };

            _apiKeys = existingApiKeys.ToDictionary(x => x.Key, x => x);
        }

        public Task<ApiKey> Execute(string providedApiKey)
        {
            _apiKeys.TryGetValue(providedApiKey, out var key);
            return Task.FromResult(key);
        }
    }
}
