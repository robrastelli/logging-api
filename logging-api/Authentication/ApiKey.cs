﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.Authentication
{
    public class ApiKey
    {
        public Guid Id { get; private set; }
        public string Owner { get; private set; }
        public string Key { get; private set; }
        public DateTimeOffset Created { get; private set; }
        public IReadOnlyCollection<string> Roles { get; private set; }

        public ApiKey(Guid id, string owner, string key, DateTimeOffset created, IReadOnlyCollection<string> roles)
        {
            Id = id;
            Owner = owner ?? throw new ArgumentNullException(nameof(owner));
            Key = key ?? throw new ArgumentNullException(nameof(key));
            Created = created;
            Roles = roles ?? throw new ArgumentNullException(nameof(roles));
        }
    }
}
