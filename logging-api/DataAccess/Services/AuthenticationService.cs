﻿using logging_api.data.authentication.DataAccess.Contexts;
using logging_api.data.authentication.DataAccess.Entities;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace logging_api.DataAccess.Services
{
    public class AuthenticationService : IAuthenticationService, IDisposable
    {
        private readonly AuthenticationContext _context;

        public AuthenticationService(AuthenticationContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        #region Organizations
        public bool OrganizationExists(Guid organizationId)
        {
            return _context.Organizations.Any(o => o.Id == organizationId);
        }

        public IEnumerable<ApiOrganization> GetOrganizations()
        {
            return _context.Organizations.ToList();
        }

        public ApiOrganization GetOrganization(Guid organizationId)
        {
            if (organizationId == null)
                throw new ArgumentNullException(nameof(organizationId));

            return _context.Organizations.FirstOrDefault(o => o.Id == organizationId);
        }

        public void AddOrganization(ApiOrganization organization)
        {
            if (organization == null)
                throw new ArgumentNullException(nameof(organization));

            organization.Id = Guid.NewGuid();

            // Add any applications provided
            if (organization.Applications.Any())
            {
                // Get the default role from the database
                var defaultRoles = _context.Roles.Where(r => r.IsDefault);

                // Add any Applications
                foreach (var app in organization.Applications)
                {
                    app.Id = Guid.NewGuid();
                    app.OrganizationId = organization.Id;
                    app.ApiKey = Guid.NewGuid().ToString();
                    app.CreateDate = DateTime.Now;

                    // Add default application role
                    foreach (var role in defaultRoles)
                    {
                        _context.ApplicationRoles.Add(new ApiApplicationRole
                        {
                            Id = Guid.NewGuid(),
                            ApplicationId = app.Id,
                            RoleId = role.Id
                        });
                    }
                }
            }

            _context.Organizations.Add(organization);
        }

        public void DeleteOrganization(ApiOrganization organization)
        {
            if (organization == null)
                throw new ArgumentNullException(nameof(organization));

            _context.Organizations.Remove(organization);
        }

        public void UpdateOrganization(ApiOrganization organization)
        {
            // Nothing to do here yet.
            // EF Covers the actual update, so long as you
            // update an Entity pulled from a _context DBSet
            // i.e. _context.Organization.FirstOrDefault(o => o.Id == organizationId).
            // updates to this object are tracked via Track Changes
        }
        #endregion Organizations

        #region Applications
        public bool ApplicationExistsForOrganization(Guid organizationId, Guid applicationId)
        {
            return _context.Applications.Any(a => a.Organization.Id == organizationId && a.Id == applicationId);
        }

        public IEnumerable<ApiApplication> GetApplicationsForOrganization(Guid organizationId)
        {
            if (organizationId == null)
                throw new ArgumentNullException(nameof(organizationId));

            return _context.Applications.Where(a => a.OrganizationId == organizationId).ToList();
        }

        public ApiApplication GetApplicationForOrganization(Guid organizationId, Guid applicationId)
        {
            return _context.Applications.FirstOrDefault(a => a.Organization.Id == organizationId && a.Id == applicationId);
        }
        #endregion Applications

        #region Api Keys
        public bool ApiKeyIsValid(string apiKey)
        {
            var application = _context.Applications.Where(a => a.ApiKey == apiKey && (a.ExpireDate == null || a.ExpireDate >= DateTime.Now));

            return application != null;
        }

        public ApiApplication GetApplicationByApiKey(string apiKey)
        {
            return _context.Applications.FirstOrDefault(a => a.ApiKey == apiKey && (a.ExpireDate == null || a.ExpireDate >= DateTime.Now));
        }

        public IEnumerable<ApiRole> GetRolesForApplication(Guid applicationId)
        {
            if (applicationId == null)
                throw new ArgumentNullException(nameof(applicationId));

            var appRoles = _context.ApplicationRoles.Where(ar => ar.ApplicationId == applicationId).ToList();
            var roles = _context.Roles.Where(r => appRoles.Select(ar => ar.RoleId).Contains(r.Id)).ToList();

            return roles;
        }
        #endregion Api Keys

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}
