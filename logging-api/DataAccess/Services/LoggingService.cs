﻿using logging_api.data.logging.DataAccess.Contexts;
using logging_api.data.logging.DataAccess.Entities;
using logging_api.Helpers;
using logging_api.ResourceParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.DataAccess.Services
{
    public class LoggingService : ILoggingService, IDisposable
    {
        private readonly LoggingContext _context;

        public LoggingService(LoggingContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<LogEntry> GetLogsForApplication(Guid applicationId)
        {
            if (applicationId == null)
                throw new ArgumentNullException(nameof(applicationId));

            return _context.LogEntries.Where(l => l.ApplicationId == applicationId).ToList();
        }

        public LogEntry GetLogForApplication(Guid applicationId, Guid logEntryId)
        {
            if (applicationId == null)
                throw new ArgumentNullException(nameof(applicationId));

            return _context.LogEntries.FirstOrDefault(l => l.ApplicationId == applicationId && l.Id == logEntryId);
        }

        public PagedList<LogEntry> GetLogsForApplications(List<Guid> applicationIds, LogResourceParameters logParams)
        {
            var query = _context.LogEntries
                .Where(l => applicationIds.Contains(l.ApplicationId) &&
                    (string.IsNullOrWhiteSpace(logParams.LogType) || logParams.LogType.Contains(l.LogLevel)));

            return PagedList<LogEntry>.Create(query, logParams.PageNumber, logParams.PageSize);
        }

        public PagedList<LogEntry> GetLogs(LogResourceParameters logParams)
        {
            var query = _context.LogEntries
                .Where(l => string.IsNullOrWhiteSpace(logParams.LogType) || logParams.LogType.Contains(l.LogLevel));

            return PagedList<LogEntry>.Create(query, logParams.PageNumber, logParams.PageSize);
        }

        public void AddLogEntry(LogEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException(nameof(entry));

            entry.Id = Guid.NewGuid();

            _context.LogEntries.Add(entry);
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}
