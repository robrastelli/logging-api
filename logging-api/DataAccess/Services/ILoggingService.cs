﻿using logging_api.data.logging.DataAccess.Entities;
using logging_api.Helpers;
using logging_api.ResourceParameters;
using System;
using System.Collections.Generic;

namespace logging_api.DataAccess.Services
{
    public interface ILoggingService
    {
        void AddLogEntry(LogEntry entry);
        LogEntry GetLogForApplication(Guid applicationId, Guid logEntryId);
        PagedList<LogEntry> GetLogs(LogResourceParameters logParams);
        IEnumerable<LogEntry> GetLogsForApplication(Guid applicationId);
        PagedList<LogEntry> GetLogsForApplications(List<Guid> applicationIds, LogResourceParameters logParams);
        bool Save();
    }
}