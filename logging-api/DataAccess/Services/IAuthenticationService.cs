﻿using logging_api.data.authentication.DataAccess.Entities;
using System;
using System.Collections.Generic;

namespace logging_api.DataAccess.Services
{
    public interface IAuthenticationService
    {
        void AddOrganization(ApiOrganization organization);
        bool ApiKeyIsValid(string apiKey);
        bool ApplicationExistsForOrganization(Guid organizationId, Guid applicationId);
        void DeleteOrganization(ApiOrganization organization);
        ApiApplication GetApplicationByApiKey(string apiKey);
        ApiApplication GetApplicationForOrganization(Guid organizationId, Guid applicationId);
        IEnumerable<ApiApplication> GetApplicationsForOrganization(Guid organizationId);
        ApiOrganization GetOrganization(Guid organizationId);
        IEnumerable<ApiOrganization> GetOrganizations();
        IEnumerable<ApiRole> GetRolesForApplication(Guid applicationId);
        bool OrganizationExists(Guid organizationId);
        bool Save();
        void UpdateOrganization(ApiOrganization organization);
    }
}