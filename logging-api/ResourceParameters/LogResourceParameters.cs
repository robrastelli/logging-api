﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.ResourceParameters
{
    public class LogResourceParameters
    {
        private const int Max_Page_Size = 100;
        private int _pageSize = 10;
        private int _pageNumber = 1;

        public int PageNumber
        {
            get => _pageNumber;
            set => _pageNumber = value < 0 ? 0 : value;
        }

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = value > Max_Page_Size ? Max_Page_Size : value;
        }

        public string OrderBy { get; set; } = "LogDate";

        public string Fields { get; set; }

        public string LogType { get; set; }
    }
}
