﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace logging_api.data.authentication.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "auth");

            migrationBuilder.CreateTable(
                name: "ApplicationRoles",
                schema: "auth",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ApplicationId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Organizations",
                schema: "auth",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organizations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                schema: "auth",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 140, nullable: false),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Applications",
                schema: "auth",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 150, nullable: false),
                    ApiKey = table.Column<string>(nullable: false),
                    CreateDate = table.Column<DateTimeOffset>(nullable: false),
                    ExpireDate = table.Column<DateTimeOffset>(nullable: true),
                    OrganizationId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Applications_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "auth",
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "auth",
                table: "ApplicationRoles",
                columns: new[] { "Id", "ApplicationId", "RoleId" },
                values: new object[] { new Guid("8553bcef-09ac-454f-9db0-3b5b6f564717"), new Guid("bf3893f8-c245-43e8-8870-b4791e6a681f"), new Guid("7106e15c-9967-485a-87dc-a5c1b31d9c4e") });

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Organizations",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("7e747ca5-7d0c-4477-927d-2d7cf0ba3afa"), "Development Test Organization" });

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Roles",
                columns: new[] { "Id", "IsDefault", "Name" },
                values: new object[,]
                {
                    { new Guid("7106e15c-9967-485a-87dc-a5c1b31d9c4e"), false, "Dev-Test Role" },
                    { new Guid("e758bdf4-c956-4aa1-b63c-69199272ccde"), false, "Admin" },
                    { new Guid("8d5e0fbe-60d1-48d8-8b30-99e7b0d1119c"), true, "Application" }
                });

            migrationBuilder.InsertData(
                schema: "auth",
                table: "Applications",
                columns: new[] { "Id", "ApiKey", "CreateDate", "ExpireDate", "Name", "OrganizationId" },
                values: new object[] { new Guid("bf3893f8-c245-43e8-8870-b4791e6a681f"), "7f3b5240-ea28-41d0-bfad-16c9cad585dc", new DateTimeOffset(new DateTime(2020, 1, 1, 10, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, -5, 0, 0, 0)), null, "Dev-Test Application", new Guid("7e747ca5-7d0c-4477-927d-2d7cf0ba3afa") });

            migrationBuilder.CreateIndex(
                name: "IX_Applications_OrganizationId",
                schema: "auth",
                table: "Applications",
                column: "OrganizationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationRoles",
                schema: "auth");

            migrationBuilder.DropTable(
                name: "Applications",
                schema: "auth");

            migrationBuilder.DropTable(
                name: "Roles",
                schema: "auth");

            migrationBuilder.DropTable(
                name: "Organizations",
                schema: "auth");
        }
    }
}
