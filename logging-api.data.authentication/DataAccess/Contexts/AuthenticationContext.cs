﻿using logging_api.data.authentication.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace logging_api.data.authentication.DataAccess.Contexts
{
    public class AuthenticationContext : DbContext
    {
        public AuthenticationContext(DbContextOptions<AuthenticationContext> options)
            : base(options)
        {
        }

        public DbSet<ApiApplication> Applications { get; set; }
        public DbSet<ApiOrganization> Organizations { get; set; }
        public DbSet<ApiRole> Roles { get; set; }
        public DbSet<ApiApplicationRole> ApplicationRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("auth");

            modelBuilder.Entity<ApiOrganization>().HasData(
                new ApiOrganization
                {
                    Id = Guid.Parse("7e747ca5-7d0c-4477-927d-2d7cf0ba3afa"),
                    Name = "Development Test Organization"
                });

            modelBuilder.Entity<ApiApplication>().HasData(
                new ApiApplication
                {
                    Id = Guid.Parse("bf3893f8-c245-43e8-8870-b4791e6a681f"),
                    Name = "Dev-Test Application",
                    ApiKey = "7f3b5240-ea28-41d0-bfad-16c9cad585dc",
                    CreateDate = DateTime.Parse("2020-01-01 10:00:00"),
                    OrganizationId = Guid.Parse("7e747ca5-7d0c-4477-927d-2d7cf0ba3afa")
                });

            modelBuilder.Entity<ApiRole>().HasData(
                new ApiRole
                {
                    Id = Guid.Parse("7106e15c-9967-485a-87dc-a5c1b31d9c4e"),
                    Name = "Dev-Test Role"
                },
                new ApiRole
                {
                    Id = Guid.Parse("e758bdf4-c956-4aa1-b63c-69199272ccde"),
                    Name = "Admin"
                },
                new ApiRole
                {
                    Id = Guid.Parse("8d5e0fbe-60d1-48d8-8b30-99e7b0d1119c"),
                    Name = "Application",
                    IsDefault = true
                });

            modelBuilder.Entity<ApiApplicationRole>().HasData(
                new ApiApplicationRole
                {
                    Id = Guid.Parse("8553bcef-09ac-454f-9db0-3b5b6f564717"),
                    ApplicationId = Guid.Parse("bf3893f8-c245-43e8-8870-b4791e6a681f"),
                    RoleId = Guid.Parse("7106e15c-9967-485a-87dc-a5c1b31d9c4e")
                });
        }
    }
}
