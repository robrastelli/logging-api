﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.data.authentication.DataAccess.Entities
{
    public class ApiOrganization
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(150)]
        public string Name { get; set; }

        public ICollection<ApiApplication> Applications { get; set; } = new List<ApiApplication>();
    }
}
