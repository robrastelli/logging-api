﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.data.authentication.DataAccess.Entities
{
    public class ApiRole
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(140)]
        public string Name { get; set; }

        [Required]
        public bool IsDefault { get; set; } = false;
    }
}
