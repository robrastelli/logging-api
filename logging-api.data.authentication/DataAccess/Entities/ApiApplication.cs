﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.data.authentication.DataAccess.Entities
{
    public class ApiApplication
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(150)]
        public string Name { get; set; }

        [Required]
        public string ApiKey { get; set; }

        [Required]
        public DateTimeOffset CreateDate { get; set; }

        public DateTimeOffset? ExpireDate { get; set; }

        [Required]
        public Guid OrganizationId { get; set; }

        [ForeignKey("OrganizationId")]
        public ApiOrganization Organization { get; set; }
    }
}
