﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace logging_api.data.logging.DataAccess.Entities
{
    public class LogEntry
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid ApplicationId { get; set; }

        [Required]
        public string LogLevel { get; set; } = "INFO";

        [Required]
        public DateTimeOffset LogDate { get; set; } = DateTime.UtcNow;

        [Required]
        public string LogMessage { get; set; }

    }
}
